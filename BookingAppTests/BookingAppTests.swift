//
//  BookingAppTests.swift
//  BookingAppTests
//
//  Created by Zenchef on 21/08/2018.
//  Copyright © 2018 zenchef. All rights reserved.
//

import XCTest
@testable import BookingApp

class BookingAppTests: XCTestCase {
    
    override func setUp() {
        super.setUp()        
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testClient() {
        let client = buildClientSUT()
        let expectation = expectation(description: "Loading booking")
        let maxUserLoading = Constant.maxUserLoading
        
        client.getBookings(maxUserLoading) { status in
            XCTAssertTrue(status, "Should return true")
            XCTAssertEqual(maxUserLoading, client.numberOfBooking(), "Should return \(maxUserLoading) booking")
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testWorkingBooking() {
        let client = buildClientSUT()
        let expectation = expectation(description: "Loading booking")
        let maxUserLoading = Constant.maxUserLoading
        
        XCTAssertNil(client.workingBookingLastName(), "The last name should be nil")
        client.getBookings(maxUserLoading) { status in
            client.setWorkingBooking(at: IndexPath(row: 0, section: 0))
            XCTAssertNotNil(client.workingBookingLastName(), "The last name should not be nil")
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    func testBookingStatus() {
        let client = buildClientSUT()
        let expectation = expectation(description: "Loading booking")
        let maxUserLoading = Constant.maxUserLoading
        
        client.getBookings(maxUserLoading) { status in
            let indexPath = IndexPath(row: 0, section: 0)
            client.setWorkingBooking(at: indexPath)
            client.updateWorkingBookingStatus(with: .confirmed)
            var status = client.bookingStatus(at: indexPath)
            XCTAssertEqual(status, .confirmed, "The status should be confirmed")
            client.updateWorkingBookingStatus(with: .waiting)
            status = client.bookingStatus(at: indexPath)
            XCTAssertEqual(status, .waiting, "The status should be waiting")
            client.updateWorkingBookingStatus(with: .cancel)
            status = client.bookingStatus(at: indexPath)
            XCTAssertEqual(status, .cancel, "The status should be cancel")
            
            
            expectation.fulfill()
        }
        wait(for: [expectation], timeout: 5.0)
    }
    
    // System Under Test
    private func buildClientSUT() -> Client {
        return Client(urlPath: Constant.testUrl)
    } 
    

}
