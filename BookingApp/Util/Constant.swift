//
//  Constant.swift
//  BookingApp
//
//  Created by Manu on 04/10/2021.
//  Copyright © 2021 zenchef. All rights reserved.
//

import Foundation

final class Constant {
    static let productionUrl = "https://randomuser.me/api/?results=%d"
    /// Test Url should be an url to a test server (for instance a Vapor server that runs locally inside a docker) 
    static let testUrl = "https://randomuser.me/api/?results=%d"
    static let maxUserLoading = 25
}
