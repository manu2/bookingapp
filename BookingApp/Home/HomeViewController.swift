//
//  ViewController.swift
//  BookingApp
//
//  Created by Zenchef on 21/08/2018.
//  Copyright © 2018 zenchef. All rights reserved.
//

import UIKit

final class HomeViewController: UIViewController {

    let label = UILabel()
    let client: Client?
    
    init(client : Client){
        self.client = client
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        client?.getBookings(Constant.maxUserLoading) { status in
            if status  == true {
                self.loadFromDatasource()
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        loadFromDatasource()
    }
    
    private func loadFromDatasource() {
        self.view.backgroundColor = client?.firstBookingState()?.color
    }
}

