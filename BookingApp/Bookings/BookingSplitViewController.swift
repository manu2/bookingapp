//
//  BookingSplitViewController.swift
//  BookingApp
//
//  Created by Manu on 04/10/2021.
//  Copyright © 2021 zenchef. All rights reserved.
//

import UIKit

final class BookingSplitViewController: UISplitViewController {
    
    private lazy var navMasterViewController: UINavigationController = {
       return UINavigationController(rootViewController: masterViewController) 
    }()
    
    private lazy var masterViewController: BookingTableViewController = {
       return BookingTableViewController(client: client)
    }()
    
    let client: Client
    
    init(client: Client) {
        self.client = client
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let detailViewController = BookingDetailViewController(client: client)
        let navigationController = UINavigationController(rootViewController: detailViewController)
        viewControllers = [navMasterViewController, navigationController]
        preferredDisplayMode = .allVisible
        masterViewController.delegate = self
        delegate = self
    }
}
extension BookingSplitViewController: UISplitViewControllerDelegate {
    func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
        return true
    }
}

extension BookingSplitViewController: BookingTableViewControllerDelegate {
    func didSelectBooking() {
        let detailViewController = BookingDetailViewController(client: client)
        detailViewController.delegate = masterViewController
        if isCollapsed {
            navMasterViewController.pushViewController(detailViewController, animated: true)
        }
        else{
            let navigationController = UINavigationController(rootViewController: detailViewController)
            showDetailViewController(navigationController, sender: self)
        }
    }
}
