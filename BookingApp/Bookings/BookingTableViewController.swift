//
//  BookingsViewController.swift
//  BookingApp
//
//  Created by Zenchef on 21/08/2018.
//  Copyright © 2018 zenchef. All rights reserved.
//

import UIKit

protocol BookingTableViewControllerDelegate: AnyObject {
    func didSelectBooking()
}

final class BookingTableViewController: UITableViewController {
    
    let client: Client
    
    weak var delegate: BookingTableViewControllerDelegate?
    
    init(client: Client) {
        self.client = client
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = .white
        self.title = NSLocalizedString("booking.table.title", comment: "")
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "reuseIdentifier")
        
        client.getBookings(Constant.maxUserLoading) { state in 
            self.tableView.reloadData()
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return client.numberOfBooking()
    }
    
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
        cell.textLabel?.text = client.bookingName(at: indexPath)
        cell.backgroundColor = client.bookingStatus(at: indexPath).color
        return cell
     }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        client.setWorkingBooking(at: indexPath)
        delegate?.didSelectBooking()
    }
}

extension BookingTableViewController: BookingDetailViewControllerDelegate {
    func didUpdate() {
        if let indexPath = client.workingIndexPath {
            tableView.reloadRows(at: [indexPath], with: .fade)
        }
    }
}
