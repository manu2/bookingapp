//
//  BooingDetailTableViewController.swift
//  BookingApp
//
//  Created by Zenchef on 21/08/2018.
//  Copyright © 2018 zenchef. All rights reserved.
//

import UIKit

protocol BookingDetailViewControllerDelegate: AnyObject {
    func didUpdate()
}

final class BookingDetailViewController: UIViewController {
    
    private var client: Client?
    
    private(set) var stackView = UIStackView()
    private(set) var firstNameLabel = UILabel()
    private(set) var lastNameLabel = UILabel()
    private(set) var statusButton = UIButton()
    
    weak var delegate: BookingDetailViewControllerDelegate?

    deinit {
        print("deinit \(self)")
    }
    
    init(client: Client?) {
        self.client = client
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        statusButton.addTarget(self, action: #selector(didPushStatusButton), for: .touchUpInside)
        
        view.addSubview(stackView)
        stackView.addArrangedSubview(firstNameLabel)
        stackView.addArrangedSubview(lastNameLabel)
        stackView.addArrangedSubview(statusButton)
        
        buildStyle()
        buildConstraints()
        loadFromDatasource()
    }
    
    private func buildStyle() {
        view.backgroundColor = .white
        stackView.alignment = .center
        stackView.distribution = .fillEqually
        stackView.axis = .vertical
        stackView.spacing = 5.0
        
        title = NSLocalizedString("booking.detail.title", comment: "")
        
        statusButton.layer.cornerRadius = 8.0
        statusButton.contentEdgeInsets = UIEdgeInsets(top: 5,left: 5,bottom: 5,right: 5)
        statusButton.setTitle(NSLocalizedString("booking.detail.button.open.status", comment: ""), for: .normal)
    }
    
    private func buildConstraints() {
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        let guide = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: guide.topAnchor),
            stackView.heightAnchor.constraint(equalToConstant: 120),
            stackView.leftAnchor.constraint(equalTo: guide.leftAnchor),
            stackView.rightAnchor.constraint(equalTo: guide.rightAnchor),
        ])
    }
    
    private func loadFromDatasource() {
        firstNameLabel.text = client?.workingBookingFirstName()
        lastNameLabel.text = client?.workingBookingLastName()
        statusButton.backgroundColor = client?.workingBookingState()?.color
    }
}

// MARK: - Actions
extension BookingDetailViewController {
    @objc
    func didPushStatusButton() {
        let statusViewController = BookingStatusViewController(client: client, 
                                                               nibName: "BookingStatusViewController", 
                                                               bundle: nil)
        statusViewController.delegate = self
        present(statusViewController, animated: true)
    }
}

// MARK: - BookingStatusViewControllerDelegate
extension BookingDetailViewController: BookingStatusViewControllerDelegate {
    func didUpdate() {
        delegate?.didUpdate()
        loadFromDatasource()
    }
}
