//
//  BookingStatusViewController.swift
//  BookingApp
//
//  Created by Zenchef on 21/08/2018.
//  Copyright © 2018 zenchef. All rights reserved.
//

import UIKit

protocol BookingStatusViewControllerDelegate: AnyObject {
    func didUpdate()
}

final class BookingStatusViewController: UIViewController {

    private var client: Client?
    
    @IBOutlet var buttonConfirmed: UIButton!
    @IBOutlet var buttonWaiting: UIButton!
    @IBOutlet var buttonCancel: UIButton!
        
    weak var delegate: BookingStatusViewControllerDelegate?
    
    deinit {
        print("deinit \(self)")
    }
    
    init(client: Client?, nibName: String?, bundle: Bundle?) {
        self.client = client
        super.init(nibName: nibName, bundle: bundle)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = NSLocalizedString("booking.detail.title", comment: "")
        
        buttonConfirmed.addTarget(self, action: #selector(confirmSelected), for: .touchUpInside)
        buttonWaiting.addTarget(self, action: #selector(waitingSelected), for: .touchUpInside)
        buttonCancel.addTarget(self, action: #selector(cancelSelected), for: .touchUpInside)

        buildStyle()
        loadFromDatasource()
    }
        
    private func loadFromDatasource() {
        view.backgroundColor = client?.workingBookingState()?.color
    }
    
    private func buildStyle() {
        buttonConfirmed.setTitle(NSLocalizedString("model.status.confirmed", comment: ""), for: .normal)
        buttonWaiting.setTitle(NSLocalizedString("model.status.waiting", comment: ""), for: .normal)
        buttonCancel.setTitle(NSLocalizedString("model.status.cancel", comment: ""), for: .normal)
    }
}

extension BookingStatusViewController {
    @objc
    func confirmSelected() {
        client?.updateWorkingBookingStatus(with: .confirmed)
        delegate?.didUpdate()
        loadFromDatasource()
    }
    
    @objc
    func waitingSelected() {
        client?.updateWorkingBookingStatus(with: .waiting)
        delegate?.didUpdate()
        loadFromDatasource()
    }
    
    @objc
    func cancelSelected() {
        client?.updateWorkingBookingStatus(with: .cancel)
        delegate?.didUpdate()
        loadFromDatasource()
    }
}
