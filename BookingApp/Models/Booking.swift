//
//  Booking.swift
//  BookingApp
//
//  Created by Zenchef on 21/08/2018.
//  Copyright © 2018 zenchef. All rights reserved.
//

import Foundation
import UIKit

final class Booking {
    var user: User
    var status: BookingStatus = .waiting
    
    init(user: User = User()) {
        self.user = user
    }
}

enum BookingStatus: String {
    case waiting
    case cancel
    case confirmed
}

extension BookingStatus {
    var color: UIColor {
        switch self {
        case .waiting: return .orange
        case .cancel: return .red
        case .confirmed: return .green
        }
    }
    
    var localized: String {
        switch self {
        case .waiting: return NSLocalizedString("model.status.waiting", comment: "")
        case .cancel: return NSLocalizedString("model.status.cancel", comment: "")
        case .confirmed: return NSLocalizedString("model.status.confirmed", comment: "") 
        }
    }
}
