//
//  User.swift
//  BookingApp
//
//  Created by Zenchef on 03/05/2021.
//  Copyright © 2021 zenchef. All rights reserved.
//

import Foundation

struct User: Decodable {
    struct Name: Decodable {
        var first: String = "John"
        var last: String = "Doe"
    }
    var name: Name = Name(first: "John", last: "Doe")
    var gender: Gender = .male
    var email: String?
    var phone: String?
}

enum Gender: String, Decodable {
    case male
    case female
}
