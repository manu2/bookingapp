//
//  Result.swift
//  BookingApp
//
//  Created by Manu on 04/10/2021.
//  Copyright © 2021 zenchef. All rights reserved.
//

import Foundation

final class Result: Decodable {
    var results = [User]()
}
