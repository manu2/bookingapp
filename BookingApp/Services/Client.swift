//
//  Client.swift
//  BookingApp
//
//  Created by Zenchef on 21/08/2018.
//  Copyright © 2018 zenchef. All rights reserved.
//

import Foundation

final class Client {
    
    let urlPath: String
    
    private var cache = [Booking]()
    private(set) var workingIndexPath: IndexPath?
    private var workingBooking: Booking?
    
    init(urlPath: String) {
        self.urlPath = urlPath
    }
    
    private static let sessionConfiguration: URLSessionConfiguration = {
        let configuration = URLSessionConfiguration.default
        // For each task, ignore the session cache, and load image from server
        configuration.requestCachePolicy = .reloadIgnoringCacheData
        return configuration
    }()
    
    private static let session: URLSession = {
        let session =  URLSession(configuration: sessionConfiguration)
        return session
    }()
    
    func getBookings(_ maxBooking: Int, completionHandler: @escaping ((_ status: Bool) -> Void)) {
        // Call API GET to https://randomuser.me/api/?results=XXX to retrieve an array of User.
        // Then create an array of bookings from the array of users
        let userPath = String(format: urlPath, maxBooking)
        guard let url = URL(string: userPath) else {
            completionHandler(false)
            return
        }
        
        if(cache.count > 0) {
            completionHandler(true)
            return
        }
        
        let task = Self.session.dataTask(with: url) { (data, response, error) in 
            if let _ = error {
                DispatchQueue.main.async {
                    completionHandler(false)                    
                }
                return
            }
            if let response = response as? HTTPURLResponse,
                               response.statusCode != 200 {
                DispatchQueue.main.async {
                    completionHandler(false)
                }
                return
            }
            if let data = data,
               let result = try? JSONDecoder().decode(Result.self, from: data) {
                let userList = result.results
                self.cache = userList.map{Booking(user: $0)}
                DispatchQueue.main.async {
                    completionHandler(true)                    
                }
                return
            }
            else {
                DispatchQueue.main.async {
                    completionHandler(false)
                }
                return
            }
        }
        task.resume()
    }
    
    func numberOfBooking() -> Int {
        return cache.count
    }
    
    func setWorkingBooking(at indexPath: IndexPath) {
        workingIndexPath = indexPath
        workingBooking = cache[indexPath.row]
    }
    
    func bookingName(at indexPath: IndexPath) -> String {
        return cache[indexPath.row].user.name.last
    }
    
    func bookingStatus(at indexPath: IndexPath) -> BookingStatus {
        return cache[indexPath.row].status
    }
    
    func firstBookingState() -> BookingStatus? {
        return cache.first?.status
    }
    
    func workingBookingState() -> BookingStatus? {
        return workingBooking?.status
    }
    
    func workingBookingFirstName() -> String? {
        return workingBooking?.user.name.first
    }
    
    func workingBookingLastName() -> String? {
        return workingBooking?.user.name.last
    }
    
    func updateWorkingBookingStatus(with status: BookingStatus) {
        workingBooking?.status = status
    }
}
